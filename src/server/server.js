require("../config/config");

const express = require("express");
const Joi = require("joi");

const User = require("../models/user");
const userJoiSchema = require("../joischemas/user");
const { comparePassword } = require("../utils/password");

const app = express();

app.use(express.json());

const PORT = process.env.PORT || 3500;

app.get("/", (req, res) => {
  User.hashPassword();
  res.send("DONE");
});

app.post("/user/register", async (req, res, next) => {
  try {
    let { email, password } = req.body;
    let { error, value } = Joi.validate({ email, password }, userJoiSchema);
    if (error) throw error;
    let savedUser = await User.findOne({ where: { email: value.email } });
    if (savedUser) throw { message: "User already exists." };
    let user = User.build(value);
    let hashedUser = await user.save();
    let token = await user.generateAuthToken();
    res
      .status(200)
      .header("x-auth", token)
      .send(hashedUser.toJson());
  } catch (e) {
    next(e);
  }
});

app.post("/user/login", async (req, res, next) => {
  try {
    let { email, password } = req.body;
    let { error, value } = Joi.validate({ email, password }, userJoiSchema);
    if (error) throw error;
    let user = await User.findOne({ where: { email: value.email } });
    let result = comparePassword(password, user.password);
    if (!result) throw { message: "Wrong username or password." };
    let token = await user.generateAuthToken();
    return res
      .status(200)
      .header("x-auth", token)
      .send(user.toJson());
  } catch (e) {
    next(e);
  }
});

app.use((err, req, res, next) => {
  console.log(err);
  res.status(400).send(err);
  next();
});

app.listen(PORT, console.log(`Auth Service started on ${PORT}`));
