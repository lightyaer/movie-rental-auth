const bcrypt = require("bcrypt");

const hashPassword = password => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, function(error, salt) {
      if (error) return reject(error);
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return reject(err);
        return resolve(hash);
      });
    });
  });
};

const comparePassword = (password, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, function(err, result) {
      if (err) return reject(err);
      return resolve(result);
    });
  });
};

module.exports = {
  hashPassword,
  comparePassword
};
