const env = process.env.NODE_ENV || "development";

if (env === "development" || env === "test") {
  let config = require("./config.json");
  let vars = config[env];
  for (let key in vars) {
    process.env[key] = vars[key];
  }
}
