const Sequelize = require("sequelize");
const db = require("../db/sequelize");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");

const { hashPassword } = require("../utils/password");

const User = db.define("users", {
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  }
});

User.beforeSave(async user => {
  if (user["_changed"]["password"]) {
    let hashedPassword = await hashPassword(user.password);
    user.password = hashedPassword;
    return user;
  }
});

User.prototype.generateAuthToken = async function() {
  let signOptions = {
    expiresIn: "12h",
    algorithm: "RS256"
  };
  let privateKey = fs.readFileSync(path.resolve(__dirname, "../config/keys/private.key"), { encoding: "utf-8", flag: "r" });
  let token = jwt.sign({ email: this.email, id: this.id }, privateKey, signOptions).toString();
  return token;
};

User.prototype.toJson = function() {
  return {
    id: this.id,
    email: this.email,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt
  };
};

module.exports = User;
