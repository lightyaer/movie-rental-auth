const Sequelize = require("sequelize");

const db = new Sequelize(process.env.DATABASE, process.env.USERNAME, process.env.PASSWORD, {
  host: process.env.HOST,
  dialect: process.env.DIALECT
});

db.authenticate()
  .then(() => console.log("Connected to DB"))
  .catch(() => console.log("Error in connecting to DB"));

module.exports = db;
