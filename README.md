# This is the Authorization Server for the Movie-Rental Resource Server.

**For Setting up this project**

- Create config.json file in src/config folder

```
{
  "development": {
    "DATABASE": "xxxxxx",
    "USERNAME": "xxxxxx",
    "PASSWORD": "xxxxxx",
    "HOST": "xxxxxx",
    "DIALECT": "xxxxxx"
  },
  "test": {
    "DATABASE": "xxxxxx",
    "USERNAME": "xxxxxx",
    "PASSWORD": "xxxxxx",
    "HOST": "xxxxxx",
    "DIALECT": "xxxxxx"
  }
}

```

- Create private.key file in src/config/keys folder and paste in your 512 bit private key.
